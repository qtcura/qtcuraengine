##  Finds the Stb utility library on your computer.
#
#   If Stb is not found on your computer, this script also gives the option to
#   download the library and build it from source.
#
#   This script exports the following parameters for use if you find the Stb
#   package:
#   - Stb_FOUND: Whether Stb has been found on your computer (or built from
#     source).
#   - Stb_INCLUDE_DIRS: The directory where the header files of Stb are located.

#First try to find a PackageConfig for this library.
find_package(PkgConfig QUIET)
pkg_check_modules(Stb QUIET stb)

if (Stb_FOUND)
    # TODO: Make check for deprecated
    set(Stb_INCLUDE_DIRS ${Stb_INCLUDE_DIRS}/deprecated)
else()
    find_path(Stb_INCLUDE_DIRS stb_image_resize.h #Search for something that is a little less prone to false positives than just stb.h.
        HINTS ${PC_Stb_INCLUDEDIR} ${PC_Stb_INCLUDEDIR}/deprecated ${PC_Stb_INCLUDE_DIRS}
        PATHS "$ENV{PROGRAMFILES}/stb" "$ENV{PROGRAMW6432}/stb"
        PATH_SUFFIXES include/stb stb include
    )
endif()

include(FindPackageHandleStandardArgs)
set(_stb_find_required ${Stb_FIND_REQUIRED}) #Temporarily set to optional so that we don't get a message when it's not found but you want to build from source.
set(_stb_find_quietly ${Stb_FIND_QUIETLY})
set(Stb_FIND_REQUIRED FALSE)
set(Stb_FIND_QUIETLY TRUE)
find_package_handle_standard_args(Stb DEFAULT_MSG Stb_INCLUDE_DIRS)
set(Stb_FIND_REQUIRED ${_stb_find_required})
set(Stb_FIND_QUIETLY ${_stb_find_quietly})
